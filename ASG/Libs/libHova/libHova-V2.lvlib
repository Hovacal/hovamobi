﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="16008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(^!!!*Q(C=\&gt;8"&lt;2MR%!813:"$A*T51;!7JA7VI";G"6V^6!P4AFJ1#^/#7F!,TN/'-(++=IC2(-TVS+O`80+:3[QDNP9VYEO]0GP@@NM_LD_\`K4&amp;2`NI`\;^0.WE\\ZH0]8D2;2'N3K6]:DK&gt;?1D(`H)2T\SFL?]Z3VP?=N,8P+3F\TE*5^ZSF/?]J3H@$PE)1^ZS*('Z'/C-?A99(2'C@%R0--T0-0D;QT0]!T0]!S0,D%]QT-]QT-]&lt;IPB':\B':\B-&gt;1GG?W1]QS0Y;.ZGK&gt;ZGK&gt;Z4"H.UQ"NMD:Q'Q1DWM6WUDT.UTR/IXG;JXG;JXF=DO:JHO:JHO:RS\9KP7E?BZT(-&amp;%]R6-]R6-]BI\C+:\C+:\C-6U54`%52*GQ$)Y1Z;&lt;3I8QJHO,R+YKH?)KH?)L(J?U*V&lt;9S$]XDE0-E4`)E4`)EDS%C?:)H?:)H?1Q&lt;S:-]S:-]S7/K3*\E3:Y%3:/;0N*A[=&lt;5+18*YW@&lt;,&lt;E^J&gt;YEO2U2;`0'WJ3R.FOM422L=]2[[,%?:KS(&amp;'PR9SVKL-7+N1CR`LB9[&amp;C97*0%OPH2-?Y_&lt;_KK,OKM4OKI$GKP&gt;I^&lt;`X,(_`U?N^MNLN&gt;L8#[8/*`0=4K&gt;YHA]RO&amp;QC0V_(\P&gt;\OUV].XR^E,Y_6Z[=@YH^5\`3`_$&gt;W.]DF`(N59`!/&lt;!-PQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">369131520</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="Private" Type="Folder">
		<Item Name="Class Hova" Type="Folder">
			<Item Name="initMFCs.vi" Type="VI" URL="../../../Classes/Hova/Public/Methods/initMFCs.vi"/>
		</Item>
		<Item Name="Events" Type="Folder">
			<Item Name="Hova A_sp.vi" Type="VI" URL="../libHova/Private/Events/Hova A_sp.vi"/>
			<Item Name="Hova Com.end.vi" Type="VI" URL="../libHova/Private/Events/Hova Com.end.vi"/>
			<Item Name="Hova File Read.vi" Type="VI" URL="../libHova/Private/Events/Hova File Read.vi"/>
			<Item Name="Hova File Write.vi" Type="VI" URL="../libHova/Private/Events/Hova File Write.vi"/>
			<Item Name="Hova Gas_All.vi" Type="VI" URL="../libHova/Private/Events/Hova Gas_All.vi"/>
			<Item Name="Hova Gases.vi" Type="VI" URL="../libHova/Private/Events/Hova Gases.vi"/>
			<Item Name="Hova Get Gas All.vi" Type="VI" URL="../libHova/Private/Events/Hova Get Gas All.vi"/>
			<Item Name="Hova Get Poly.vi" Type="VI" URL="../libHova/Private/Events/Hova Get Poly.vi"/>
			<Item Name="Hova Init.vi" Type="VI" URL="../libHova/Private/Events/Hova Init.vi"/>
			<Item Name="Hova Max.vi" Type="VI" URL="../libHova/Private/Events/Hova Max.vi"/>
			<Item Name="Hova MFC_e.vi" Type="VI" URL="../libHova/Private/Events/Hova MFC_e.vi"/>
			<Item Name="Hova Prepare.vi" Type="VI" URL="../libHova/Private/Events/Hova Prepare.vi"/>
			<Item Name="Hova Set Pctrl.vi" Type="VI" URL="../libHova/Private/Events/Hova Set Pctrl.vi"/>
			<Item Name="Hova Set Poly.vi" Type="VI" URL="../libHova/Private/Events/Hova Set Poly.vi"/>
			<Item Name="Hova Update.vi" Type="VI" URL="../libHova/Private/Events/Hova Update.vi"/>
		</Item>
		<Item Name="to xyGraph" Type="Folder">
			<Item Name="Hova xyGraph Append.vi" Type="VI" URL="../libHova/Private/to xyGraph/Hova xyGraph Append.vi"/>
		</Item>
		<Item Name="Hova Append.vi" Type="VI" URL="../libHova/Private/Hova Append.vi"/>
		<Item Name="Hova Build Cmd.vi" Type="VI" URL="../libHova/Private/Hova Build Cmd.vi"/>
		<Item Name="Hova Check Answer.vi" Type="VI" URL="../libHova/Private/Hova Check Answer.vi"/>
		<Item Name="Hova Check ID.vi" Type="VI" URL="../libHova/Private/Hova Check ID.vi"/>
		<Item Name="Hova Check Written Data.vi" Type="VI" URL="../libHova/Private/Hova Check Written Data.vi"/>
		<Item Name="Hova Cmd to Str.vi" Type="VI" URL="../libHova/Private/Hova Cmd to Str.vi"/>
		<Item Name="Hova Cntrl Cmds.vi" Type="VI" URL="../libHova/Private/Hova Cntrl Cmds.vi"/>
		<Item Name="Hova ComData.vi" Type="VI" URL="../libHova/Private/Hova ComData.vi"/>
		<Item Name="Hova ComServer.vi" Type="VI" URL="../libHova/Private/Hova ComServer.vi"/>
		<Item Name="Hova CRC.vi" Type="VI" URL="../libHova/Private/Hova CRC.vi"/>
		<Item Name="Hova Cyclic Read.vi" Type="VI" URL="../libHova/Private/Hova Cyclic Read.vi"/>
		<Item Name="Hova End Read.vi" Type="VI" URL="../libHova/Private/Hova End Read.vi"/>
		<Item Name="Hova Eval File Read.vi" Type="VI" URL="../libHova/Private/Hova Eval File Read.vi"/>
		<Item Name="Hova Eve to Str.vi" Type="VI" URL="../libHova/Private/Hova Eve to Str.vi"/>
		<Item Name="Hova Ident Gases FF.vi" Type="VI" URL="../libHova/Private/Hova Ident Gases FF.vi"/>
		<Item Name="Hova Ident Gases FN.vi" Type="VI" URL="../libHova/Private/Hova Ident Gases FN.vi"/>
		<Item Name="Hova Init Queue.vi" Type="VI" URL="../libHova/Private/Hova Init Queue.vi"/>
		<Item Name="Hova Scan Read String.vi" Type="VI" URL="../libHova/Private/Hova Scan Read String.vi"/>
		<Item Name="Hova Send Cmd.vi" Type="VI" URL="../libHova/Private/Hova Send Cmd.vi"/>
		<Item Name="Hova Serial Read.vi" Type="VI" URL="../libHova/Private/Hova Serial Read.vi"/>
		<Item Name="Hova Serial WR 05171601.vi" Type="VI" URL="../libHova/Private/Hova Serial WR 05171601.vi"/>
		<Item Name="Hova Serial WR 06091601.vi" Type="VI" URL="../libHova/Private/Hova Serial WR 06091601.vi"/>
		<Item Name="Hova Serial WR.vi" Type="VI" URL="../libHova/Private/Hova Serial WR.vi"/>
		<Item Name="Hova String 2 Num.vi" Type="VI" URL="../libHova/Private/Hova String 2 Num.vi"/>
		<Item Name="Hova String to Hex.vi" Type="VI" URL="../libHova/Private/Hova String to Hex.vi"/>
	</Item>
	<Item Name="Public" Type="Folder">
		<Item Name="Conversions" Type="Folder">
			<Item Name="mL 2 L.vi" Type="VI" URL="../libHova/Public/Conversions/mL 2 L.vi"/>
		</Item>
		<Item Name="Event Send" Type="Folder">
			<Item Name="Hova Event Send Cmd.vi" Type="VI" URL="../libHova/Public/Hova Event Send Cmd.vi"/>
			<Item Name="Hova Event Send Hova.vi" Type="VI" URL="../libHova/Public/Hova Event Send Hova.vi"/>
			<Item Name="Hova Event Send.vi" Type="VI" URL="../libHova/Public/Hova Event Send.vi"/>
		</Item>
		<Item Name="TopLvl VI" Type="Folder">
			<Item Name="Hova Event Cmd 2 String.vi" Type="VI" URL="../libHova/Public/TopLvl VI/Hova Event Cmd 2 String.vi"/>
			<Item Name="Hova TopLvl VI Cmds.ctl" Type="VI" URL="../libHova/Public/TopLvl VI/Hova TopLvl VI Cmds.ctl"/>
			<Item Name="Hova TopLvl VI User Event Data.ctl" Type="VI" URL="../libHova/Public/TopLvl VI/Hova TopLvl VI User Event Data.ctl"/>
			<Item Name="Hova User Event TopLvl VI.vi" Type="VI" URL="../libHova/Public/TopLvl VI/Hova User Event TopLvl VI.vi"/>
		</Item>
		<Item Name="Hova Backend.vi" Type="VI" URL="../libHova/Public/Hova Backend.vi"/>
		<Item Name="Hova Check Com.vi" Type="VI" URL="../libHova/Public/Hova Check Com.vi"/>
		<Item Name="Hova Edit ini.vi" Type="VI" URL="../libHova/Public/Hova Edit ini.vi"/>
		<Item Name="Hova Read ini.vi" Type="VI" URL="../libHova/Public/Hova Read ini.vi"/>
		<Item Name="Hova Show File.vi" Type="VI" URL="../libHova/Public/Hova Show File.vi"/>
		<Item Name="Hova Sort Files.vi" Type="VI" URL="../libHova/Public/Hova Sort Files.vi"/>
	</Item>
	<Item Name="TypeDefs" Type="Folder">
		<Item Name="Exp Num Indicator.ctl" Type="VI" URL="../libHova/TypeDefs/Exp Num Indicator.ctl"/>
		<Item Name="Hova Answers.ctl" Type="VI" URL="../libHova/TypeDefs/Hova Answers.ctl"/>
		<Item Name="Hova Cmds.ctl" Type="VI" URL="../libHova/TypeDefs/Hova Cmds.ctl"/>
		<Item Name="Hova ComData Cluster.ctl" Type="VI" URL="../libHova/TypeDefs/Hova ComData Cluster.ctl"/>
		<Item Name="Hova CRC.ctl" Type="VI" URL="../libHova/TypeDefs/Hova CRC.ctl"/>
		<Item Name="Hova Cyclic Cluster.ctl" Type="VI" URL="../libHova/TypeDefs/Hova Cyclic Cluster.ctl"/>
		<Item Name="Hova Event Data.ctl" Type="VI" URL="../libHova/TypeDefs/Hova Event Data.ctl"/>
		<Item Name="Hova Event Ref.ctl" Type="VI" URL="../libHova/TypeDefs/Hova Event Ref.ctl"/>
		<Item Name="Hova Events.ctl" Type="VI" URL="../libHova/TypeDefs/Hova Events.ctl"/>
		<Item Name="Hova File Rights.ctl" Type="VI" URL="../libHova/TypeDefs/Hova File Rights.ctl"/>
		<Item Name="Hova Global Data Action.ctl" Type="VI" URL="../libHova/TypeDefs/Hova Global Data Action.ctl"/>
		<Item Name="Hova Req Data.ctl" Type="VI" URL="../libHova/TypeDefs/Hova Req Data.ctl"/>
	</Item>
	<Item Name="Hova Conv Read String.vi" Type="VI" URL="../libHova/Private/Hova Conv Read String.vi"/>
	<Item Name="Hova File Find.vi" Type="VI" URL="../libHova/Private/Events/Hova File Find.vi"/>
	<Item Name="Hova filePlot Append.vi" Type="VI" URL="../libHova/Private/to filePlot/Hova filePlot Append.vi"/>
	<Item Name="Hova Find Match Answer.vi" Type="VI" URL="../libHova/Private/Hova Find Match Answer.vi"/>
	<Item Name="Hova Get lcl All.vi" Type="VI" URL="../libHova/Private/Events/Hova Get lcl All.vi"/>
	<Item Name="Hova Get Min All.vi" Type="VI" URL="../libHova/Private/Events/Hova Get Min All.vi"/>
	<Item Name="Hova Ident Files FF.vi" Type="VI" URL="../libHova/Private/Hova Ident Files FF.vi"/>
	<Item Name="Hova Ident Files FN.vi" Type="VI" URL="../libHova/Private/Hova Ident Files FN.vi"/>
	<Item Name="Hova Init2.vi" Type="VI" URL="../libHova/Private/Events/Hova Init2.vi"/>
	<Item Name="Hova MFM Run.vi" Type="VI" URL="../libHova/Private/Events/Hova MFM Run.vi"/>
	<Item Name="Hova Serial Read w.o event.vi" Type="VI" URL="../libHova/Private/Hova Serial Read w.o event.vi"/>
	<Item Name="Hova Serial Write.vi" Type="VI" URL="../libHova/Private/Hova Serial Write.vi"/>
	<Item Name="Hova Set Active.vi" Type="VI" URL="../libHova/Private/Events/Hova Set Active.vi"/>
	<Item Name="Hova Set MFC_sp.vi" Type="VI" URL="../libHova/Private/Events/Hova Set MFC_sp.vi"/>
	<Item Name="Hova Set Min All.vi" Type="VI" URL="../libHova/Private/Events/Hova Set Min All.vi"/>
	<Item Name="libHova-Errors.xlsx" Type="Document" URL="../libHova/libHova-Errors.xlsx"/>
</Library>
